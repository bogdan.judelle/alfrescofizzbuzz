package com.judele.alfresco;


import org.junit.Rule;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;

public class FizzBuzzGameShould {

    private static final String STRING_FIZZ_7_8_FIZZ = "fizz 7 8 fizz";
    private static final String STRING_19_BUZZ = "19 buzz";
    private static final String STRING_2_FIZZ_4_BUZZ_FIZZ_7_8_FIZZ_BUZZ_11_FIZZ_13_14
            = "2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14";

    private static final String STRING_13_14_FIZZBUZZ_16_17
            = "2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14";

    private static final String STRING_1_2_FIZZ_4_BUZZ_FIZZ_7_8_FIZZ_BUZZ_11_FIZZ_13_14_FIZZBUZZ_16_17_FIZZ_19_BUZZ
            = "1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz 16 17 fizz 19 buzz";

    @Rule
    public ExpectedException expectation = ExpectedException.none();

    private final FizzBuzzGame fizzBuzzGame = new FizzBuzzGame();

    @Test
    public void displayTheNumberItself() {
        int leftSideInterval = 41;
        int rightSideInterval = leftSideInterval + anyInt();
        String expectedPrefix = String.valueOf(leftSideInterval);

        String result = fizzBuzzGame.play(leftSideInterval, rightSideInterval);

        assertTrue(result.startsWith(expectedPrefix));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 22, 14, -41, 17, 12224})
    public void displayTheNumberItself(int leftSideInterval) {
        int rightSideInterval = leftSideInterval + anyInt();
        String expectedPrefix = String.valueOf(leftSideInterval);

        String result = fizzBuzzGame.play(leftSideInterval, rightSideInterval);
        assertTrue(result.startsWith(expectedPrefix));
    }

    @Test
    public void displayFizzForNumbersThatAreMultipleOfThree() {
        int leftSideInterval = 6;
        int rightSideInterval = 9;

        String result = fizzBuzzGame.play(leftSideInterval, rightSideInterval);

        assertEquals(STRING_FIZZ_7_8_FIZZ, result);
    }

    @Test
    public void displayBuzzForNumbersThatAreMultipleOfFive() {
        int leftSideInterval = 19;
        int rightSideInterval = 20;

        String result = fizzBuzzGame.play(leftSideInterval, rightSideInterval);

        assertEquals(STRING_19_BUZZ, result);
    }

    @Test
    public void displayFizzAndBuzzForNumbersThatAreMultipleOfThreeAndFiveRespectively() {
        int leftSideInterval = 2;
        int rightSideInterval = 14;

        String result = fizzBuzzGame.play(leftSideInterval, rightSideInterval);

        assertEquals(STRING_2_FIZZ_4_BUZZ_FIZZ_7_8_FIZZ_BUZZ_11_FIZZ_13_14, result);
    }

    @Test
    public void displayFizzBuzzForNumbersThatAreMultipleOfFifteen() {
        int leftSideInterval = 2;
        int rightSideInterval = 14;

        String result = fizzBuzzGame.play(leftSideInterval, rightSideInterval);

        assertEquals(STRING_13_14_FIZZBUZZ_16_17, result);
    }

    @Test
    public void displayCorrectlyFizzAndBuzzAndFizzbuzz() {
        int leftSideInterval = 1;
        int rightSideInterval = 20;

        String result = fizzBuzzGame.play(leftSideInterval, rightSideInterval);

        assertEquals(STRING_1_2_FIZZ_4_BUZZ_FIZZ_7_8_FIZZ_BUZZ_11_FIZZ_13_14_FIZZBUZZ_16_17_FIZZ_19_BUZZ, result);
    }


    @Test
    public void throwIllegalArgumentExceptionForLeftSideLimitGraterThanRightSideLimit() {
        int leftSideInterval = 3;
        int rightSideInterval = 2;
        expectation.expect(IllegalArgumentException.class);

        fizzBuzzGame.play(leftSideInterval, rightSideInterval);
    }

}