package com.judele.alfresco;

public class FizzBuzzGame {

    private final TranslationCenter translationCenter;

    public FizzBuzzGame() {
        translationCenter = new TranslationCenter();
    }

    public String play(int leftSideLimit, int rightSideLimit) {
        if (leftSideLimit > rightSideLimit ) {
            throw new IllegalArgumentException();
        }
        return play(0, leftSideLimit, new Object[rightSideLimit - leftSideLimit + 1]);
    }

    private String play(int currentIndex, int currentNumber, Object[] result) {
        if (currentIndex == result.length) {
            return new ResultConvertor(result).asString();
        }
        result[currentIndex] = translationCenter.translate(currentNumber);
        return play(++currentIndex, ++currentNumber, result);
    }


}
